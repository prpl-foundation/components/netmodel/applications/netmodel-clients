/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <netmodel/client.h>

#include "netmodel-clients.h"

#define ME "netmodel-clients"

static nmc_app_t app;

static amxm_shared_object_t* get_mib_so(const char* mib) {
    amxm_shared_object_t* so = NULL;
    const char* nm_client_dir = GETP_CHAR(&(nmc_get_parser()->config), "netmodel_client_dir");
    amxc_string_t str;
    int ret = 0;

    amxc_string_init(&str, 0);

    SAH_TRACEZ_INFO(ME, "Going to find so for mib %s", mib);
    so = amxm_get_so(mib);
    when_not_null(so, exit);

    amxc_string_setf(&str, "%s/mib_%s.so", nm_client_dir, mib);
    SAH_TRACEZ_INFO(ME, "Going to open so for mib %s", mib);
    ret = amxm_so_open(&so, mib, amxc_string_get(&str, 0));
    if(ret != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to open %s", amxc_string_get(&str, 0));
    }

exit:
    amxc_string_clean(&str);
    return so;
}

void mib_event_handler(UNUSED const char* const sig_name,
                       const amxc_var_t* const data,
                       UNUSED void* const priv) {
    const char* mib = GETP_CHAR(data, "mib");
    const char* fn = NULL;
    const char* notification = GETP_CHAR(data, "notification");

    SAH_TRACEZ_INFO(ME, "mib event received: %s", notification);

    when_str_empty_trace(mib, exit, ERROR, "Trying to use netmodel client with an empty mib name");
    when_str_empty_trace(notification, exit, ERROR, "Notification is empty");
    fn = (strcmp(notification, "dm:mib-added") == 0) ? "mib-added" : "mib-removed";

    when_failed_trace(invoke_amxm_function(mib, fn, data), exit, ERROR, "Invoking %s for mib %s failed", fn, mib);

exit:
    return;
}

static void process_existing_mibs(amxc_var_t* data) {
    const char* intf = amxc_var_key(data);
    const char* mibs = amxc_var_constcast(cstring_t, data);
    amxc_string_t str;
    amxc_var_t event;
    int ret = 0;
    amxc_var_t var;
    const char* path = NULL;

    amxc_var_init(&var);
    amxc_var_init(&event);
    amxc_var_set_type(&event, AMXC_VAR_ID_HTABLE);
    amxc_string_init(&str, 0);

    when_str_empty_trace(mibs, exit, INFO, "Trying to process empty mib list for intf %s", intf);

    amxc_string_setf(&str, "NetModel.Intf.%s.", intf);
    amxc_var_add_key(cstring_t, &event, "object", amxc_string_get(&str, 0));

    ret = amxb_get(netmodel_get_amxb_bus(), amxc_string_get(&str, 0), 0, &var, 5);
    when_failed_trace(ret, exit, ERROR, "Failed to get path for %s", amxc_string_get(&str, 0));
    path = amxc_var_key(GETP_ARG(&var, "0.0"));
    when_str_empty_trace(path, exit, ERROR, "Failed to get path for %s", amxc_string_get(&str, 0));
    amxc_var_add_key(cstring_t, &event, "path", path);

    amxc_var_add_key(cstring_t, &event, "notification", "dm:mib-added");

    amxc_var_clean(&var);
    amxc_string_setf(&str, "%s", mibs);
    amxc_string_ssv_to_var(&str, &var, NULL);
    amxc_var_for_each(tmp, &var) {
        amxc_var_set_key(&event, "mib", tmp, AMXC_VAR_FLAG_UPDATE);
        mib_event_handler(NULL, &event, NULL);
    }

exit:
    amxc_var_clean(&var);
    amxc_var_clean(&event);
    amxc_string_clean(&str);
}

static void nmc_init(amxd_dm_t* dm,
                     amxo_parser_t* parser) {
    int ret = 0;
    amxc_var_t* data = NULL;

    SAH_TRACEZ_INFO(ME, "started");
    app.dm = dm;
    app.parser = parser;

    when_false_trace(netmodel_initialize(), exit, ERROR, "Failed to initialize NetModel");

    ret = amxb_subscribe(netmodel_get_amxb_bus(),
                         "NetModel.",
                         "notification matches 'dm:mib-*' && path matches 'NetModel\\.Intf\\..*'",
                         mib_event_handler,
                         NULL);
    when_failed_trace(ret, exit, ERROR, "Failed to subscribe for mib events");

    SAH_TRACEZ_INFO(ME, "Succesfully registered for mib-* events");

    SAH_TRACEZ_INFO(ME, "Going to get existing MIB info");
    data = netmodel_getMibs("resolver", "", netmodel_traverse_all);
    when_null_trace(data, exit, ERROR, "Failed to get current MIBs from NetModel");

    SAH_TRACEZ_INFO(ME, "Processing existing MIBs");
    amxc_var_for_each(var, data) {
        process_existing_mibs(var);
    }

    amxc_var_delete(&data);

exit:
    return;
}

static void nmc_exit(UNUSED amxd_dm_t* dm,
                     UNUSED amxo_parser_t* parser) {
    app.dm = NULL;
    app.parser = NULL;

    amxb_unsubscribe(netmodel_get_amxb_bus(), "NetModel.", mib_event_handler, NULL);

    amxm_close_all();
    netmodel_cleanup();

    SAH_TRACEZ_INFO(ME, "stopped");
}

int invoke_amxm_function(const char* const mib,
                         const char* fn,
                         const amxc_var_t* const data) {
    amxm_shared_object_t* so = get_mib_so(mib);
    amxc_var_t ret;
    amxc_var_t args;
    int rv = -1;

    amxc_var_init(&ret);
    amxc_var_init(&args);
    when_null_trace(so, exit, ERROR, "Could not find library for mib %s", mib);

    amxc_var_copy(&args, data);
    SAH_TRACEZ_INFO(ME, "Going to call %s for mib %s", fn, mib);
    when_failed_trace(amxm_execute_function(mib, mib, fn, &args, &ret), exit, INFO, "Execution of %s failed", fn);

    rv = 0;

exit:
    amxc_var_clean(&ret);
    amxc_var_clean(&args);
    return rv;
}

amxd_dm_t* PRIVATE nmc_get_dm(void) {
    return app.dm;
}

amxo_parser_t* PRIVATE nmc_get_parser(void) {
    return app.parser;
}

int _nmc_main(int reason,
              amxd_dm_t* dm,
              amxo_parser_t* parser) {
    SAH_TRACEZ_INFO(ME, "entry point %s, reason: %d", __func__, reason);
    switch(reason) {
    case 0:     // START
        nmc_init(dm, parser);
        break;
    case 1:     // STOP
        nmc_exit(dm, parser);
        break;
    default:
        break;
    }

    return 0;
}

