# netmodel-clients

## Summary

Plugin to load the NetModel client libraries

## Description

The NetModel client code must run in a different process than the NetModel core code.
This is necessary to avoid circular dependencies between components.
