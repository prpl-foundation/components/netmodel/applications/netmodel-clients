# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.2.2 - 2024-07-23(07:35:43 +0000)

### Fixes

- Better shutdown script

## Release v0.2.1 - 2024-04-10(10:02:43 +0000)

### Changes

- Make amxb timeouts configurable

## Release v0.2.0 - 2023-10-09(07:06:51 +0000)

### New

- [amxrt][no-root-user][capability drop]Netmodel/Netmodel-clients must be adapted to run as non-root and lmited capabilities

## Release v0.1.10 - 2023-08-24(10:15:32 +0000)

### Fixes

- Init script has no shutdown function

## Release v0.1.9 - 2023-03-14(19:07:38 +0000)

### Fixes

- Fix syntax in init script

## Release v0.1.8 - 2023-03-09(12:04:29 +0000)

### Other

- [Config] enable configurable coredump generation

## Release v0.1.7 - 2022-11-28(15:35:04 +0000)

### Other

- [NetModel] Update copyright information

## Release v0.1.6 - 2022-10-25(10:38:46 +0000)

### Fixes

- Use the correct cleanup function to avoid memory leaks

## Release v0.1.5 - 2022-08-22(14:31:27 +0000)

### Other

- [netmodel] sometimes netmodel-clients won't start

## Release v0.1.4 - 2022-06-20(12:37:16 +0000)

### Other

- [PRPLoS] Reduce logging during start up.

## Release v0.1.3 - 2022-03-24(10:18:12 +0000)

### Changes

- [GetDebugInformation] Add data model debuginfo in component services

## Release v0.1.2 - 2022-03-10(11:26:28 +0000)

### Other

- [NetModel-clients] Add unit tests

## Release v0.1.1 - 2022-02-28(13:39:46 +0000)

### Other

- Opensource component

## Release v0.1.0 - 2022-02-28(10:47:16 +0000)

### New

- [NetModel] Create separate plugin to run NetModel clients

